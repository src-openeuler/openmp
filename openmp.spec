%bcond_without sys_llvm
%bcond_with check
%bcond_without toolchain_clang
%bcond_without bisheng_autotuner

%if %{with toolchain_clang}
%global toolchain clang
%endif

%global toolchain clang

%global maj_ver 17
%global min_ver 0
%global patch_ver 6
%global openmp_version %{maj_ver}.%{min_ver}.%{patch_ver}
%global openmp_srcdir openmp-%{openmp_version}.src

%if %{with sys_llvm}
%global pkg_name libomp
%global install_prefix %{_prefix}
%global install_datadir %{_datadir}
%else
%global pkg_name libomp%{maj_ver}
%global install_prefix %{_libdir}/llvm%{maj_ver}
%global install_datadir %{install_prefix}/share
%endif

%global install_includedir %{install_prefix}/include
%global install_libdir %{install_prefix}/%{_lib}
%global install_libexecdir %{install_prefix}/libexec
%global install_sharedir %{install_prefix}/share

%ifarch ppc64le
%global libomp_arch ppc64
%else
%global libomp_arch %{_arch}
%endif

Name:    %{pkg_name}
Version: %{openmp_version}
Release: 5
Summary: OpenMP runtime for clang

License: Apache-2.0
URL:     https://openmp.llvm.org
Source0: https://github.com/llvm/llvm-project/releases/download/llvmorg-%{openmp_version}/%{openmp_srcdir}.tar.xz

BuildRequires: gcc gcc-c++
BuildRequires: cmake
BuildRequires: ninja-build
BuildRequires: elfutils-libelf-devel
BuildRequires: perl
BuildRequires: perl-Data-Dumper
BuildRequires: perl-Encode
BuildRequires: libffi-devel
%if "%toolchain" == "clang"
BuildRequires: clang
BuildRequires: clang-tools-extra
%endif

%if %{with sys_llvm}
BuildRequires: llvm-devel llvm-test
BuildRequires: llvm-cmake-utils = %{version}
%else
BuildRequires: llvm%{maj_ver}-devel
BuildRequires: llvm%{maj_ver}-test
BuildRequires: llvm%{maj_ver}-cmake-utils = %{version}
%endif

Requires: elfutils-libelf%{?isa}

%description
OpenMP runtime for clang.
 
%package devel
Summary: OpenMP header files
Requires: %{name}%{?isa} = %{version}-%{release}
%if %{with sys_llvm}
Requires: clang-resource-filesystem%{?isa} = %{version}
%else
Requires: clang%{maj_ver}-resource-filesystem%{?isa} = %{version}
%endif
 
%description devel
OpenMP header files.

%prep
%autosetup -n %{openmp_srcdir} -p2

%build
 
%cmake	-GNinja \
	-DCMAKE_INSTALL_PREFIX=%{install_prefix} \
	-DLIBOMP_INSTALL_ALIASES=OFF \
	-DCMAKE_MODULE_PATH=%{install_datadir}/llvm/cmake/Modules \
	-DLLVM_DIR=%{install_libdir}/cmake/llvm \
	-DCMAKE_INSTALL_INCLUDEDIR=%{install_libdir}/clang/%{maj_ver}/include \
%if 0%{?__isa_bits} == 64
	-DOPENMP_LIBDIR_SUFFIX=64 \
%else
	-DOPENMP_LIBDIR_SUFFIX= \
%endif
%if "%{toolchain}" == "clang"
	-DCMAKE_C_COMPILER=clang \
	-DCMAKE_CXX_COMPILER=clang++ \
%endif
%if %{with bisheng_autotuner}
	-DLLVM_ENABLE_AUTOTUNER=ON \
%endif
	-DCMAKE_SKIP_RPATH:BOOL=ON

%cmake_build
 
%install
%cmake_install
rm -rf %{buildroot}%{install_libdir}/libarcher_static.a

%files
%license LICENSE.TXT
%{install_libdir}/libomp.so
%{install_libdir}/libompd.so
%ifnarch %{arm}
%{install_libdir}/libarcher.so
%endif
%ifnarch %{ix86} %{arm} riscv64 loongarch64
%{install_libdir}/libomptarget.rtl.amdgpu.so.%{maj_ver}
%{install_libdir}/libomptarget.rtl.cuda.so.%{maj_ver}
%{install_libdir}/libomptarget.rtl.%{libomp_arch}.so.%{maj_ver}
%endif
%{install_libdir}/libomptarget.so.%{maj_ver}
 
%files devel
%{install_libdir}/clang/%{maj_ver}/include/omp.h
%{install_libdir}/cmake/openmp/FindOpenMPTarget.cmake
%ifnarch %{arm}
%{install_libdir}/clang/%{maj_ver}/include/omp-tools.h
%{install_libdir}/clang/%{maj_ver}/include/ompt.h
%{install_libdir}/clang/%{maj_ver}/include/ompt-multiplex.h
%endif
%ifnarch %{ix86} %{arm}
%ifnarch riscv64 loongarch64
%{install_libdir}/libomptarget.rtl.amdgpu.so
%{install_libdir}/libomptarget.rtl.cuda.so
%{install_libdir}/libomptarget.rtl.%{libomp_arch}.so
%endif
%{install_libdir}/libomptarget.devicertl.a
%{install_libdir}/libomptarget-amdgpu-*.bc
%{install_libdir}/libomptarget-nvptx-*.bc
%endif
%{install_libdir}/libomptarget.so
 
%changelog
* Sun Nov 10 2024 Funda Wang <fundawang@yeah.net> - 17.0.6-5
- adopt to new cmake macro

* Tue Aug 20 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-4
- Add BiSheng Autotuner support.

* Fri Jul 5 2024 liyunfei <liyunfei33@huawei.com> - 17.0.6-3
- Add toolchain_clang build support

* Wed Jun 05 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 17.0.6-2
- fix build error for loongarch64

* Mon Dec 4 2023 zhoujing <zhoujing106@huawei.com> - 17.0.6-1
- Update to 17.0.6

* Sat Aug 26 2023 cf-zhao <zhaochuanfeng@huawei.com> - 12.0.1-1
- Initial version to 12.0.1
